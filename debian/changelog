changeo (1.3.0-3) unstable; urgency=medium

  * Team upload.
  * Fix Python3.12 string syntax
    Closes: #1085416
  * Build-Depends: s/dh-python/dh-sequence-python3/ (routine-update)

 -- Andreas Tille <tille@debian.org>  Thu, 05 Dec 2024 17:39:21 +0100

changeo (1.3.0-2) unstable; urgency=medium

  * Team upload.
  * rm-setuptools.patch: new: remove dependency on setuptools.
    (Closes: #1083334)
  * d/control: declare compliance to standards version 4.7.0.

 -- Étienne Mollier <emollier@debian.org>  Wed, 09 Oct 2024 16:54:09 +0200

changeo (1.3.0-1) unstable; urgency=medium

  * Team upload.
  * New upstream version
    Closes: #1028449
  * Standards-Version: 4.6.2 (routine-update)
  * Fix cut-n-pasto in autopkgtest
  * Refresh checksums in autopkgtest to match output of new version

 -- Andreas Tille <tille@debian.org>  Wed, 11 Jan 2023 11:01:43 +0100

changeo (1.2.0-1) unstable; urgency=medium

  * New upstream version 1.2.0
  * Run cme

 -- Nilesh Patra <nilesh@debian.org>  Mon, 01 Nov 2021 23:47:51 +0530

changeo (1.1.0-1) unstable; urgency=medium

  * New upstream version 1.1.0
  * d/tests/run-unit-tests: Change checksums in output

 -- Nilesh Patra <nilesh@debian.org>  Sun, 15 Aug 2021 16:40:05 +0530

changeo (1.0.2-2~0exp0) experimental; urgency=medium

  * Team Upload.
  * Add autopkgtests. (Closes: #970597)

 -- Shruti Sridhar <shruti.sridhar99@gmail.com>  Mon, 29 Mar 2021 22:20:49 +0530

changeo (1.0.2-1) unstable; urgency=medium

  * New upstream version
  * Standards-Version: 4.5.1 (routine-update)

 -- Nilesh Patra <npatra974@gmail.com>  Tue, 19 Jan 2021 18:40:10 +0530

changeo (1.0.1-1) unstable; urgency=medium

  * Team upload.
  * New upstream version
  * debhelper-compat 13 (routine-update)
  * Set upstream metadata fields: Repository.
  * Add manpages

 -- Nilesh Patra <npatra974@gmail.com>  Thu, 15 Oct 2020 13:05:52 +0000

changeo (1.0.0-1) unstable; urgency=medium

  * New upstream version
  * Rules-Requires-Root: no (routine-update)

 -- Steffen Moeller <moeller@debian.org>  Wed, 13 May 2020 12:24:52 +0200

changeo (0.4.6-2) unstable; urgency=medium

  * Team upload.
  * Source only upload
    Closes: #953321
  * Standards-Version: 4.5.0 (routine-update)
  * Add salsa-ci file (routine-update)
  * Use secure URI in Homepage field.

 -- Andreas Tille <tille@debian.org>  Sun, 08 Mar 2020 08:25:27 +0100

changeo (0.4.6-1) unstable; urgency=medium

  * New upstream version
  * debhelper-compat 12
  * Standards-Version: 4.4.0
  * Created with Andreas' routine-update script

 -- Steffen Moeller <moeller@debian.org>  Sat, 27 Jul 2019 12:03:49 +0200

changeo (0.4.5-1) unstable; urgency=medium

  * Team upload.
  * Maintenance in Debian Med team
  * cme fix dpkg-control
  * debhelper 12
  * Remove trailing whitespace in debian/copyright

 -- Andreas Tille <tille@debian.org>  Fri, 11 Jan 2019 22:37:16 +0100

changeo (0.4.4-1) unstable; urgency=medium

  * New upstream release.

 -- Steffen Moeller <moeller@debian.org>  Wed, 28 Nov 2018 11:25:42 +0100

changeo (0.3.12-2) unstable; urgency=medium

  * Added python3-pip as build-dependency. Many thanks to Santiago Vila
    for reporting (Closes: #913107).

 -- Steffen Moeller <moeller@debian.org>  Thu, 08 Nov 2018 12:44:08 +0100

changeo (0.3.12-1) unstable; urgency=medium

  * Initial release (Closes: #901742).

 -- Steffen Moeller <moeller@debian.org>  Sun, 17 Jun 2018 21:14:11 +0200
